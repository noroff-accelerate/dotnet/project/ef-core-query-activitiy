﻿using Microsoft.EntityFrameworkCore;
using Noroff.Samples.EfCore.Queries.Data;

namespace Noroff.Samples.EfCore.Queries
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var context = new UniversityContext();
            // EnsureCreated is because we have no migrations because we are using in-memory.
            context.Database.OpenConnection();
            context.Database.EnsureCreated();

            var students = context.Students.ToList();
            foreach ( var student in students )
            {
                Console.WriteLine( student.Name );
            }
        }
    }
}