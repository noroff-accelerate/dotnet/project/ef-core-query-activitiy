﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Noroff.Samples.EfCore.Queries.Data.Entities
{
    public class Course
    {
        public int CourseId { get; set; }
        public string Title { get; set; }
        public int InstructorId { get; set; }
        public Instructor Instructor { get; set; }
        public int Credits { get; set; }
        public List<Enrollment> Enrollments { get; set; }
    }
}
