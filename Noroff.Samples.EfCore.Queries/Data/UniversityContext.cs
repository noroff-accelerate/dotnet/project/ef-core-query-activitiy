﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Noroff.Samples.EfCore.Queries.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace Noroff.Samples.EfCore.Queries.Data
{
    public class UniversityContext : DbContext
    {
        public DbSet<Student> Students { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Instructor> Instructors { get; set; }
        public DbSet<Enrollment> Enrollments { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source= :memory:");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
            modelBuilder.Entity<Student>().HasData(
                new Student { StudentId = 1, Name = "Alice", EnrollmentDate = new DateTime(2022, 9, 1), GPA = 3.7 },
                new Student { StudentId = 2, Name = "Bob", EnrollmentDate = new DateTime(2022, 9, 1), GPA = 3.9 },
                new Student { StudentId = 3, Name = "Charlie", EnrollmentDate = new DateTime(2022, 9, 1), GPA = 3.5 },
                new Student { StudentId = 4, Name = "David", EnrollmentDate = new DateTime(2022, 9, 1), GPA = 3.8 },
                new Student { StudentId = 5, Name = "Eva", EnrollmentDate = new DateTime(2022, 9, 1), GPA = 3.6 },
                new Student { StudentId = 6, Name = "Frank", EnrollmentDate = new DateTime(2022, 9, 1), GPA = 3.4 },
                new Student { StudentId = 7, Name = "Grace", EnrollmentDate = new DateTime(2022, 9, 1), GPA = 3.7 },
                new Student { StudentId = 8, Name = "Helen", EnrollmentDate = new DateTime(2022, 9, 1), GPA = 3.9 },
                new Student { StudentId = 9, Name = "Ivy", EnrollmentDate = new DateTime(2022, 9, 1), GPA = 3.5 },
                new Student { StudentId = 10, Name = "Jack", EnrollmentDate = new DateTime(2022, 9, 1), GPA = 3.8 }
            );

            modelBuilder.Entity<Course>().HasData(
                new Course { CourseId = 1, Title = "Mathematics", InstructorId = 1, Credits = 3 },
                new Course { CourseId = 2, Title = "Physics", InstructorId = 2, Credits = 4 },
                new Course { CourseId = 3, Title = "Chemistry", InstructorId = 3, Credits = 3 },
                new Course { CourseId = 4, Title = "Computer Science", InstructorId = 4, Credits = 4 },
                new Course { CourseId = 5, Title = "Biology", InstructorId = 5, Credits = 3 },
                new Course { CourseId = 6, Title = "History", InstructorId = 6, Credits = 3 },
                new Course { CourseId = 7, Title = "Literature", InstructorId = 7, Credits = 3 },
                new Course { CourseId = 8, Title = "Art", InstructorId = 8, Credits = 2 },
                new Course { CourseId = 9, Title = "Music", InstructorId = 9, Credits = 2 },
                new Course { CourseId = 10, Title = "Economics", InstructorId = 10, Credits = 3 }
            );


            modelBuilder.Entity<Instructor>().HasData(
                new Instructor { InstructorId = 1, Name = "Professor Smith", Department = "Math", Courses = new List<Course>() },
                new Instructor { InstructorId = 2, Name = "Dr. Johnson", Department = "Physics", Courses = new List<Course>() },
                new Instructor { InstructorId = 3, Name = "Professor Williams", Department = "Chemistry", Courses = new List<Course>() },
                new Instructor { InstructorId = 4, Name = "Dr. Brown", Department = "Computer Science", Courses = new List<Course>() },
                new Instructor { InstructorId = 5, Name = "Professor Davis", Department = "Biology", Courses = new List<Course>() },
                new Instructor { InstructorId = 6, Name = "Dr. Miller", Department = "History", Courses = new List<Course>() },
                new Instructor { InstructorId = 7, Name = "Professor Wilson", Department = "Literature", Courses = new List<Course>() },
                new Instructor { InstructorId = 8, Name = "Dr. Moore", Department = "Art", Courses = new List<Course>() },
                new Instructor { InstructorId = 9, Name = "Professor Taylor", Department = "Music", Courses = new List<Course>() },
                new Instructor { InstructorId = 10, Name = "Dr. Anderson", Department = "Economics", Courses = new List<Course>() }
            );



            modelBuilder.Entity<Enrollment>().HasData(
                new Enrollment { EnrollmentId = 1, StudentId = 1, CourseId = 1, Grade = 90 },
                new Enrollment { EnrollmentId = 2, StudentId = 2, CourseId = 1, Grade = 88 },
                new Enrollment { EnrollmentId = 3, StudentId = 3, CourseId = 2, Grade = 92 },
                new Enrollment { EnrollmentId = 4, StudentId = 4, CourseId = 2, Grade = 85 },
                new Enrollment { EnrollmentId = 5, StudentId = 5, CourseId = 3, Grade = 87 },
                new Enrollment { EnrollmentId = 6, StudentId = 6, CourseId = 3, Grade = 91 },
                new Enrollment { EnrollmentId = 7, StudentId = 7, CourseId = 4, Grade = 88 },
                new Enrollment { EnrollmentId = 8, StudentId = 8, CourseId = 4, Grade = 90 },
                new Enrollment { EnrollmentId = 9, StudentId = 9, CourseId = 5, Grade = 89 },
                new Enrollment { EnrollmentId = 10, StudentId = 10, CourseId = 5, Grade = 93 }
            );


        }
    }

}
